from fastapi import FastAPI, Request, File, UploadFile, Depends
from pydantic import BaseModel
import pandas as pd
from pprint import pprint
import json
from faker import Faker

app = FastAPI()
faker = Faker()

@app.get("/")
def home():
    return {"app": "file upload project"}

@app.get("/users/{number}")
def read_item(number: int = 10):
    users = [faker.simple_profile() for _ in range(number)]
    return users

@app.post("/uploadfile")
async def create_upload_file(data: UploadFile = File(...)):
    """_summary_

    :param data: _description_, defaults to File(...)
    :type data: UploadFile, optional
    :return: _description_
    :rtype: _type_
    """
    print(data.filename)
    # print(data.file.readlines())
    df = pd.read_csv(data.file)
    json_data = df.to_json(orient="records")
    file_data = json.loads(json_data)
    return {"Filename": data.filename, "Records": len(file_data), "FileData": file_data}
